use std::ops::div_pow2;

enum Output<T> {
    UpdateMode,
    FrameInv{val: bool},
    AllClear,
    Dummy,
    Address{b: bool},
    Pixel{p: T},
    // Chip select should be high but sclk should be masked
    CsHigh,
    // Chip select should be low but sclk should be masked
    CsLow
}

struct DisplayGenOutput {
    sclk: bool,
    o: Output<(uint<10>, uint<9>)>
}

/// Generates the base otuput signals which should then be converted
/// to contain the graphics and passed to the output translator
entity display_gen(clk: clock, rst: bool, t: Timing) -> DisplayGenOutput {
    let Clk$(sclk, sclk_falling_edge) = inst clk_gen(clk, rst, t);

    let state = inst display_fsm$(clk, rst, sclk_falling_edge, t);

    DisplayGenOutput(sclk, display_output(state))
}

struct OutputPins {
    sclk: bool,
    cs: bool,
    mosi: bool,
}

fn output_translator(o: Output<bool>, sclk: bool) -> OutputPins {
    match o {
        Output::UpdateMode => OutputPins$(sclk, cs: true, mosi: true),
        Output::FrameInv(val) => OutputPins$(sclk, cs: true, mosi: val),
        Output::AllClear => OutputPins$(sclk, cs: true, mosi: false),
        Output::Dummy => OutputPins$(sclk, cs: true, mosi: false),
        Output::Address(val) => OutputPins$(sclk, cs: true, mosi: val),
        Output::Pixel(val) => OutputPins$(sclk, cs: true, mosi: val),
        Output::CsHigh => OutputPins$(sclk: false, cs: true, mosi: false),
        Output::CsLow => OutputPins$(sclk: false, cs: false, mosi: false),
    }
}

// Private stuff

struct Timing {
    us1: uint<10>,
    us3: uint<10>,
    mhz1: uint<10>
}

// Using int<9> to avoid having to worry about negative numbers
enum State {
    CsLow{line: uint<9>, duration: uint<10>},
    CsHigh{line: uint<9>, duration: uint<10>},
    Mode{line: uint<9>},
    FrameInv{line: uint<9>},
    Clear{line: uint<9>},
    FirstDummy{line: uint<9>, count: uint<5>},
    Address{line: uint<9>, count: uint<5>},
    Data{line: uint<9>, x: uint<10>},
    EndDummy{line: uint<9>, count: uint<5>},
    EndCsh{line: uint<9>, duration: uint<10>},
}


struct Clk {
    sclk: bool,
    sclk_falling_edge: bool
}

entity clk_gen(clk: clock, rst: bool, t: Timing) -> Clk {
    reg(clk) ctr reset(rst: 0) = {
        if ctr == t.mhz1 {
            0
        }
        else {
            trunc(ctr+1)
        }
    };

    Clk$(
        sclk: ctr > (t.mhz1 >> 1),
        sclk_falling_edge: ctr == 0
    )
}

entity display_fsm(clk: clock, rst: bool, sclk_falling_edge: bool, t: Timing) -> State {
    reg(clk) state reset(rst: State::CsLow(0, 0)) = match (sclk_falling_edge, state) {
        (_, State::CsLow$(line, duration)) => {
            if duration == trunc(t.us1 - 1) {
                State::CsHigh$(line, duration: 0)
            }
            else {
                State::CsLow$(line, duration: trunc(duration+1))
            }
        },
        (_, State::CsHigh$(line, duration)) => {
            if duration == trunc(t.us1 - 1) {
                if sclk_falling_edge {State::Mode(line)} else {state}
            }
            else {
                State::CsHigh$(line, duration: trunc(duration+1))
            }
        },
        (true, State::Mode(line)) => State::FrameInv(line),
        (true, State::FrameInv(line)) => State::Clear(line),
        (true, State::Clear(line)) => State::FirstDummy$(line, count: 0),
        (true, State::FirstDummy$(line, count: 4)) => State::Address$(line, count: 0),
        (true, State::FirstDummy$(line, count)) => State::FirstDummy$(line, count: trunc(count+1)),
        (true, State::Address$(line, count: 7)) => State::Data$(line, x: 0),
        (true, State::Address$(line, count)) => State::Address$(line, count: trunc(count + 1)),
        (true, State::Data$(line, x: 399)) => State::EndDummy$(line, count: 0),
        (true, State::Data$(line, x)) => State::Data$(line, x: trunc(x+1)),
        (true, State::EndDummy$(line, count: 15)) => State::EndCsh$(line, duration: 0),
        (true, State::EndDummy$(line, count)) => State::EndDummy$(line, count: trunc(count+1)),
        (_, State::EndCsh$(line, duration)) => {
            if duration >= trunc(t.us1 - 1) {
                let new_line = if line == 239 { 0 } else { trunc(line+1) };
                State::CsLow$(line: new_line, duration: 0)
            }
            else {
                State::EndCsh$(line, duration: trunc(duration+1))
            }
        },
        (false, _) => state,
    };

    state
}


fn display_output(state: State) -> Output<(uint<10>, uint<9>)> {
    match state {
        State::CsLow(_, _) => Output::CsLow(),
        State::CsHigh(_, _) => Output::CsHigh(),
        State::Mode(_) => Output::UpdateMode(),
        State::FirstDummy(_, _) => Output::Dummy(),
        State::FrameInv(line) => Output::FrameInv((line & 1) == 1),
        State::Clear(_) => Output::AllClear(),
        State::Address(line, count) => Output::Address(((line >> zext(count)) & 1) == 1),
        State::Data$(line, x) => Output::Pixel((x, line)),
        State::EndDummy(_, _) => Output::Dummy(),
        State::EndCsh(_, _) => Output::CsHigh(),
    }
}
